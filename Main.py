# coding: utf-8
#This is the main file. Run this, to run the program
import random
import pygame
from pygame.locals import *
import math
import sys
import time

import Menu
import Map
#Setup
pygame.init()
pygame.font.init()
#colors
black 	= (0	,0		,0)
red		= (255	,0		,0)
green	= (0	,255	,0)
yellow	= (255	,255	,0)
blue	= (0	,0		,255)
pink	= (255	,0		,255)
cyan	= (0	,255	,255)
white 	= (255	,255	,255)
# set up fonts
basicFont = pygame.font.SysFont(None, 48)

FPS = 60 # 60 frames per second
clock = pygame.time.Clock()

global done
global buttons
done = False
font = pygame.font.SysFont("Calibri", 15)

#window
SHEIGTH  = int(480)+100
SWIDTH = int(480*math.sqrt(2))+200
global MainWindow
MainWindow = pygame.display.set_mode((SWIDTH, SHEIGTH))
	
def mainmenuSU(): #Setting up the Main Menu
	global buttons
	global Function
	MainWindow.fill(white)
	pygame.display.flip()		
	buttons = [Menu.Button(250,120,200,100,gameSU,"Generate terrain")]
	Function = mainmenu
def mainmenu():
	global buttons
	global Function
	MainWindow.fill(white)
	keys = pygame.key.get_pressed()
	for event in pygame.event.get(): # User did something
		if event.type == pygame.QUIT or keys[K_ESCAPE]: # Closed from X
			global done 
			done = True # Stop the Loop
		if event.type == pygame.MOUSEBUTTONUP:
			for button in buttons:#check if any in game buttons are pressed
				button.pressed(pygame.mouse.get_pos())
	for button in buttons:
		button.draw()
	pygame.display.flip()		
	
def gameSU(): #Setting up the game
	time_s = time.time()
	global map
	global shore
	global lakes
	global mapimage
	global cur
	global Function
	global buttons
	lakes = []
	shore = []
	print "Creating the map, bear with me for a moment"
	map = Map.world_gen_Pangea()
	#map = Map.world_gen_shitfuck()
	print "Creating general terrain info"
	for array in map:
		for tile in array:
			tile.set_tile()
	print "Erosion. sorry, this can take a while"
	Map.erosion(map,1)
	print "defining sea. Sorry again" 
	Map.set_sea(map[0][0])
	print "Sea defined, doing lakes." 
	for array in map:
		for tile in array:
			if tile.terrain == 'water':
			#	print tile.pos
				lakes.append(Map.set_lakes(tile))
	print str(len(lakes))+' lakes formed. or something like that'
	print "humidity. oh god, you have no idea how sorry I am ;-;"
	Map.humidity(shore)
	print "setting temperatures. Please don't hit me ,_,"
	Map.temperature(map)
	seconds = int(time.time() - time_s)
	print "It took",seconds,"seconds to get map done : ) \n \n"
	MainWindow.fill(white)

	mapimage = Map.Maim()
	cur = Menu.Cursor()
	for array in map:
		for tile in array:
			tile.draw()
	buttons = [Menu.Button(1,SHEIGTH-99,98,98,mainmenuSU,"Menu"), Menu.MMButton(100,SHEIGTH-99,99,98,0,"Terrain",mapimage.ter_map),
		Menu.MMButton(200,SHEIGTH-99,99,98,0,"Temperature",mapimage.tem_map),Menu.MMButton(300,SHEIGTH-99,99,98,0,"Humidity",mapimage.hum_map)]
	mapimage.draw([0,0])
	Menu.draw_sbars() #draw sidebars
	pygame.display.flip()
	Function = game
def game():
	cur.pos = pygame.mouse.get_pos()
	keys = pygame.key.get_pressed()
	for event in pygame.event.get(): # User did something
		if event.type == pygame.QUIT or keys[K_ESCAPE] : # Closed from X
			global done 
			done = True # Stop the Loop
		if event.type == pygame.MOUSEBUTTONUP:
			cur.pressed()
			for button in buttons:#check if any in game buttons are pressed
				button.pressed(pygame.mouse.get_pos())
			if event.dict['button'] == 4:
				mapimage.zoom(1)
			elif event.dict['button'] == 5:
				mapimage.zoom(-1)
	for button in buttons:
		button.draw()
	offset = [0,0]
	from Map import size
	if keys[K_LEFT]:
		offset[0] += size
	elif keys[K_RIGHT]:
		offset[0] -= size
	if keys[K_UP]:
		offset[1] += size
	elif keys[K_DOWN]:
		offset[1] -= size
	#if not offset == [0,0]:
	mapimage.draw(offset)
	pygame.display.update(0,0,SWIDTH-200,SHEIGTH-100)
	cur.draw()
def main():
	global Function
	mainmenuSU()
	while not done:
		Function()
		clock.tick(FPS)
		pygame.display.set_caption("FPS: %i" % clock.get_fps())	
main()

pygame.quit()
sys.exit(0)