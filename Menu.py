# -*- coding: cp1252 -*-
import pygame, sys

import Civ

from pygame.locals import *
import math

black 	= (0	,0		,0)
red		= (255	,0		,0)
green	= (0	,255	,0)
yellow	= (255	,255	,0)
blue	= (0	,0		,255)
pink	= (255	,0		,255)
cyan	= (0	,255	,255)
white 	= (255	,255	,255)
pygame.font.init()

def draw_sbars():
	from Main import MainWindow,SHEIGTH,SWIDTH
	pygame.draw.rect(MainWindow,(2,4,7),(0,SHEIGTH-100,SWIDTH,100),0)
	pygame.draw.rect(MainWindow,(13,24,44),(SWIDTH-200,0,SWIDTH,SHEIGTH),0)
	
class Cursor (object):
	def __init__(self):
		from Map import size
		self.pos = (0,0)
		self.sca = size
		self.rect = pygame.Rect(0,0,size,size)
	def draw(self):
		pos = ((self.pos[0]/self.sca)*self.sca,(self.pos[1]/self.sca)*self.sca)
		from Main import SWIDTH,SHEIGTH
		if not pos[1]>=(SHEIGTH-100) and not pos[0]>=(SWIDTH-200): 
			from Main import MainWindow, mapimage, map
			c =  (self.rect.topleft[0]+mapimage.pos[0],self.rect.topleft[1]+mapimage.pos[1])
			MainWindow.blit(mapimage.ter_map,c, self.rect)
			pygame.display.update(self.rect)
			self.rect.topleft = pos
			pygame.draw.rect(MainWindow,(255,60,60),self.rect,1)	#Draw the cursor
			pygame.display.update(self.rect)
			font = pygame.font.SysFont("Calibri", 15)
			x = (self.rect.topleft[0]-mapimage.pos[0])/self.sca
			y = (self.rect.topleft[1]-mapimage.pos[1])/self.sca
			map[x][y].info()
			if self.pos[0] > (SWIDTH-(200+self.sca)) or self.pos[1] > (SHEIGTH-(100+self.sca)):
				draw_sbars()
	def pressed(self):
		from Main import SWIDTH,SHEIGTH
		if self.pos[0] > (SWIDTH-200) and self.pos[1] > (SHEIGTH-100):
			pos = ((self.pos[0]/self.sca)*self.sca,(self.pos[1]/self.sca)*self.sca)
			from Main import map,mapimage
			x = (self.rect.topleft[0]-mapimage.pos[0])/self.sca
			y = (self.rect.topleft[1]-mapimage.pos[1])/self.sca
			print ("tile ", [x,y]," was pressed")			 

class Button (object):	#Menu Button
	def __init__(self,x,y,w,l,funk,text):
		self.pos = [x,y]
		self.rect = pygame.Rect(x,y,w,l)
		self.text = text
		self.font = pygame.font.SysFont("Calibri", 15)
		self.label = self.font.render(self.text, 1, black)
		self.Function = funk #this means that this button should change the main loop into "Self.Function" when pressed
	def pressed(self, mouse):
		if self.rect.collidepoint(mouse):
			self.Function()
	def draw(self):
		from Main import MainWindow
		pygame.draw.rect(MainWindow, blue,self.rect,0)
		MainWindow.blit(self.label, (self.pos[0],self.pos[1]+(self.rect.height/2)))

class MMButton (object):#Mapmode button 
	def __init__(self,x,y,w,l,num,text,map):
		self.map = map
		self.pos = [x,y]
		self.rect = pygame.Rect(x,y,w,l)
		self.text = text
		self.font = pygame.font.SysFont("Calibri", 15)
		self.label = self.font.render(self.text, 1, black)
		self.num = num
	def pressed(self, mouse):
		if self.rect.collidepoint(mouse):
			from Main import mapimage
			mapimage.map = self.map
			mapimage.zoom (0) #sets new map to be in the same size as the current map is in
	def draw(self):
		from Main import MainWindow
		pygame.draw.rect(MainWindow, blue,self.rect,0)
		MainWindow.blit(self.label, (self.pos[0],self.pos[1]))
