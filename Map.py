import pygame
import random
import perlin
import math
global map_size
black 	= (0	,0		,0)
red		= (255	,0		,0)
lgreen	= (0	,255	,0)
green	= (0	,155	,0)
dgreen	= (0	,55		,0)
yellow	= (255	,255	,0)
blue	= (0	,0		,255)
pink	= (255	,0		,255)
cyan	= (0	,255	,255)
white 	= (255	,255	,255)
pygame.font.init()

class Maim (object):		#this is base surface of terrain
	def __init__(self):
		from Main import SWIDTH, SHEIGTH
		global xlen, ylen, size
		self.screen = (SWIDTH-200, SHEIGTH-100)
		self.x = xlen*size
		self.y = ylen*size
		self.ter_map = pygame.Surface((self.x, self.y),0)
		self.hum_map = pygame.Surface((self.x, self.y),0)
		self.tem_map = pygame.Surface((self.x, self.y),0)
		self.map = self.ter_map
		self.pos = [0,0]
		
	def draw (self,os):
		os = [os[0]*size,os[1]*size]
		res = map(sum,zip(self.pos,os))
		if (res[0] > 0 or res[0] < self.screen[0]-self.x):	#These two ifs make sure the image does not go over the limits
			res[0] = self.pos[0]
		if (res[1] > 0 or res[1] < self.screen[1]-self.y):
			res[1] = self.pos[1]
		self.pos = res
		from Main import MainWindow,SWIDTH, SHEIGTH
		MainWindow.blit(self.map,(self.pos[0],self.pos[1]),((0,0),(SWIDTH-200-self.pos[0], SHEIGTH-100-self.pos[1])))
		
	def zoom (self,m):
		global xlen, ylen, size
		from Main import cur
		size += m
		if size > 10:
			size = 10
			return
		if size < 2:
			size = 2
			return
		cur.sca = size
		cur.rect = pygame.Rect(0,0,cur.sca,cur.sca)
		self.x = xlen*size
		self.y = ylen*size
		c = pygame.mouse.get_pos()
		if m > 0:
			self.pos = [(self.pos[0]-(c[0]-self.pos[0]))/2,(self.pos[1]-(c[1]-self.pos[1]))/2]
		else:
			self.pos = [(self.pos[0]+(c[0]+self.pos[0]))/2,(self.pos[1]+(c[1]+self.pos[1]))/2] 
		self.pos = [self.pos[0]/size*size,self.pos[1]/size*size]
		if (self.pos[0] < self.screen[0]-self.x):	#These two ifs make sure the image does not go over the limits
			self.pos[0] = self.screen[0]-self.x
		elif (self.pos[0] > 0):
			self.pos[0] = 0
		if (self.pos[1] < self.screen[1]-self.y):
			self.pos[1] = self.screen[1]-self.y
		elif (self.pos[1] > 0):
			self.pos[1] = 0
		self.map = pygame.transform.scale(self.map,(self.x, self.y))
		
	
class Tile (object):
	def __init__(self,x,y,hg):
		global size
		self.colour = white
		self.pos = [x,y]
		self.rect = pygame.Rect(self.pos[0]*size, self.pos[1]*size ,size ,size)
		self.heigth = round(hg,0)
		self.food = 1
		self.movcost = 1
		self.fscore = 1 # h score(estimated cost)
		self.next = []	# nodes next to this one go in heres
		self.terrain = None
		self.flags = []
		self.RGO = []
		self.humidity = 0
		
	def set_tile(self):
		global map_size
		from Main import map
		for n in range(8):
			self.next.append(None)
		self.next[0] = map[self.pos[0]-1][self.pos[1]] 			#left
		self.next[1] = map[self.pos[0]][self.pos[1]-1] 			#up
		self.next[4] = map[self.pos[0]-1][self.pos[1]-1]		#upleft
		if not self.pos[0] == map_size[0]-1 and not self.pos[1] == map_size[1]-1:
			self.next[2] = map[self.pos[0]+1][self.pos[1]] 		#rigth	
			self.next[3] = map[self.pos[0]][self.pos[1]+1] 		#down
			self.next[5] = map[self.pos[0]+1][self.pos[1]-1]	#uprigth		# 4 1 5																									
			self.next[6] = map[self.pos[0]+1][self.pos[1]+1]	#downrigth		# 0 x 2
			self.next[7] = map[self.pos[0]-1][self.pos[1]+1]	#downleft		# 7 3 6
		else:
			if self.pos[0] == map_size[0]-1:
				self.next[2] = map[0][self.pos[1]] 			#rigth	
				self.next[5] = map[0][self.pos[1]-1]			#uprigth																									
				if self.pos[1] == map_size[1]-1:
					self.next[6] = map[0][0]					#downrigth
					self.next[3] = map[self.pos[0]][0] 		#down
					self.next[7] = map[self.pos[0]-1][0]		#downleft
				else:
					self.next[3] = map[self.pos[0]][self.pos[1]+1] 		#down
					self.next[6] = map[0][self.pos[1]+1]				#downrigth	
					self.next[7] = map[self.pos[0]-1][self.pos[1]+1]	#downleft
			elif self.pos[1] == map_size[1]-1:
				self.next[3] = map[self.pos[0]][0]				#down
				self.next[7] = map[self.pos[0]-1][0]			#downleft	
				self.next[6] = map[self.pos[0]+1][0]			#downrigth
				self.next[2] = map[self.pos[0]+1][self.pos[1]] 	#rigth	
				self.next[5] = map[self.pos[0]+1][self.pos[1]-1]#uprigths
		if self.heigth > 0:
			self.terrain = 'grass'
		else:
			self.terrain = 'water'
	def define (self):
		if self.temp < -10:			#is it ice?
			self.terrain = 'ice'
			self.RGO.append([10,.1,'food'])
			self.heigth = random.randint(1,10)
			self.colour =  (255-1*self.heigth,255-1*self.heigth,255-1*self.heigth)
		elif self.heigth > 0:
			if self.heigth > 200:
				self.terrain =='mountain'
				if self.heigth > 255:
					self.heigth = 255
			if self.terrain == 'mountain':
				self.RGO = [500,.01,'stone']
				x = (255-1*self.heigth)/2
				self.colour =  (x,x,x)
			elif self.terrain == 'sand':
				self.RGO.append([200,.3,'food'])
				self.colour =  (255-1*self.heigth,255-1*self.heigth,100)
			elif self.terrain == 'cliff':
				self.RGO.append([50,.2,'food'])
				x = (255-1*self.heigth)/2
				self.colour =  (x,x,x)
			else:
				if self.temp >= 20:		#if region is hot
					if self.humidity < 3:
						self.terrain = 'desert'
						self.colour =  (255-self.heigth,255-self.heigth,100)
					elif self.humidity < 7:
						self.RGO.append([50,.1,'food'])
						self.terrain = 'savannah'
						R = int(self.heigth/2)
						G = int(self.heigth/2)
						self.colour =  (160-R,210-G,10)	
					else:
						self.terrain = 'rainforest'
						self.RGO.append([300,.5,'wood'])
						self.RGO.append([35,.4,'food'])
						R = int(self.heigth/3)
						G = int(self.heigth/2)
						self.colour =  (90-R,150-G,0)
				elif self.temp >= 10:		#if region is temperate
					if self.humidity < 1:
						self.terrain = 'dry plains'
						self.RGO.append([50,.3,'food'])
						R = self.heigth
						G = self.heigth
						B = self.heigth/2
						self.colour =  (255-R,255-G,150-B)						
					elif self.humidity < 3:
						self.terrain = 'pineforest'
						self.RGO.append([300,.4,'wood'])
						self.RGO.append([20,.4,'food'])
						G = self.heigth/2
						self.colour =  (10,135-G,10)
					elif self.humidity < 4:
						self.terrain = 'grass plains'
						self.RGO.append([50,.5,'food'])
						self.colour =  (40,255-1*self.heigth,40)
					else:
						self.terrain = 'mapleforest'
						self.RGO.append([250,.4,'wood'])
						self.RGO.append([40,.5,'food'])
						G = self.heigth/2
						self.colour =  (20,255-G,20)
				elif self.temp >= 0:		#if region is cold
					if self.humidity < 5:
						self.terrain = 'steppe'
						self.RGO.append([50,.3,'food'])
						R = self.heigth
						G = self.heigth
						B = self.heigth/2
						self.colour =  (255-R,255-G,150-B)
					else:
						self.terrain = 'pineforest'
						self.RGO.append([300,.4,'wood'])
						self.RGO.append([20,.4,'food'])
						G = self.heigth/2
						self.colour =  (10,135-G,10)
				elif self.temp >= -10:
					self.terrain = 'tundra'
					self.RGO.append([40,.1,'wood'])
					self.RGO.append([40,.1,'food'])
					R = self.heigth/2
					self.colour = (140-R,140-R,130-R)	
				else:
					self.colour =  (40,255-1*self.heigth,40)
					self.RGO.append([50,.3,'food'])
					self.terrain = 'grass'
		else:
			self.flags.append("LIQUID")
			if self.heigth < -255:
				self.heigth = -255
			if self.terrain == 'sea':
				self.colour =  (0,(255-int(math.fabs(self.heigth)))/5,255-1*math.fabs(self.heigth))
			else:
				self.colour =  (0,0,255-1*math.fabs(self.heigth))
				#self.terrain = 'lake'			
	def draw(self):
		from Main import mapimage
		pygame.draw.rect(mapimage.ter_map,self.colour,self.rect,0)
		pygame.draw.rect(mapimage.hum_map,(25*self.humidity,10,10),self.rect,0)
		if self.temp > 0:
			pygame.draw.rect(mapimage.tem_map,(40+math.fabs(self.temp)*5,10,10),self.rect,0)
		else:
			pygame.draw.rect(mapimage.tem_map,(10,10,40+math.fabs(self.temp)*5),self.rect,0)
	def info(self):
		self.font = pygame.font.SysFont("Calibri", 15)
		from Main import MainWindow,SWIDTH,SHEIGTH
		pygame.draw.rect(MainWindow,(13,24,44),(SWIDTH-200,0,SWIDTH,SHEIGTH),0)
		text = ("Tile position: " + str(self.pos))
		label = self.font.render(text, 1, white)	
		MainWindow.blit(label, (SWIDTH-190,15))
		text = ("Terrain: " + self.terrain)
		label = self.font.render(text, 1, white)	
		MainWindow.blit(label, (SWIDTH-190,30))
		text = ("Humidity: " + str(self.humidity))
		label = self.font.render(text, 1, white)	
		MainWindow.blit(label, (SWIDTH-190,45))
		text = ("Temperatur: "+ str(self.temp))
		label = self.font.render(text, 1, white)	
		MainWindow.blit(label, (SWIDTH-190,60))
		
		pygame.display.update(SWIDTH-200,0,SWIDTH,SHEIGTH)
	def step(self):
		pass

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~tile End~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def set_sea(start):
	openl = [ ]
	openl.append(start)
	start.terrain='sea'
	while (not len(openl) == 0):
		tile = openl[-1]
		openl.remove(tile)
		tile.humidity = 8
		for x in range(4):
			if tile.next[x].terrain == 'water':
				openl.append(tile.next[x])
				tile.next[x].terrain = 'sea'
				tile.next[x].flags.append("LIQUID")
			elif tile.next[x].terrain == 'grass':
				if not "LIQUID" in tile.next[x].flags:
					from Main import shore
					shore.append(tile.next[x])
					tile.next[x].flags.append("SHORE")
					if tile.next[x].heigth <= 2:
						tile.next[x].terrain = 'sand'
						tile.humidity = 7
					elif tile.next[x].heigth > 10:
						tile.next[x].terrain = 'cliff'
						tile.humidity = 5
					else:
						tile.humidity = 6
def set_lakes(start):
	openl = [ ]
	lake = []
	openl.append(start)
	start.terrain = 'lake'
	while (not len(openl) == 0):
		tile = openl[-1]
		openl.remove(tile)
		lake.append(tile)
		tile.humidity = 10
		for x in range(4):
			if tile.next[x].terrain == 'water':
				openl.append(tile.next[x])
				tile.next[x].terrain = 'lake'
				tile.next[x].flags.append("LIQUID")
			elif not "LIQUID" in tile.next[x].flags:				
				if not "SHORE" in tile.next[x].flags:
					from Main import shore
					shore.append(tile.next[x])
					tile.next[x].humidity = 10
					tile.next[x].flags.append("SHORE")
	return lake
def humidity(shore):
	for start in shore:
		openl = []
		openl.append(start)
		for tile in openl:
			openl.remove(tile)
			for x in range(8):
				if tile.next[x].humidity < tile.humidity:
					if tile.humidity >= 0:
						diff = tile.next[x].heigth - tile.heigth
						if diff > random.randint(-2,2) + random.randint(-2,2):
							tile.next[x].humidity = tile.humidity - 1
						else:
							tile.next[x].humidity = tile.humidity
						openl.append(tile.next[x])
def temperature(map):
	global ylen
	ycen = ylen/2
	for array in map:
		for tile in array:
			yos = (tile.pos[1]-ycen)**2
			#tile.temp = 30- int((yos)/(ylen))
			tile.temp = int(30 - (yos)/(ylen)/(0.8 + tile.humidity*.1))
			if tile.temp < -30:
				tile.temp =-30
			tile.define()			
def erosion(map,m):
	for x in range (0,m):
		for array in map:
			for tile in array:
				t = 0
				for x in range(4):
					t += tile.next[x].heigth 
				t = t/4
				if t < tile.heigth:
					tile.heigth -= 1
					for x in range(4):
						tile.next[x].heigth += 1/4 
				elif t > tile.heigth:
					tile.heigth += 1
					for x in range(4):
						tile.next[x].heigth -= 1/4 	
def world_gen_Pangea():
	global xlen
	global ylen
	global size
	global map_size
	size = 3
	ylen = int(260/1)
	xlen = int(ylen*math.sqrt(2))
	#xlen = int(100)
	#ylen = int(50)
	xcen = xlen/2
	ycen = ylen/2
	map_size = [xlen,ylen]
	map = []
	Xsca = 0.54
	Ysca = 0.54
	cl=15   #length of the continent
	cw=20	#width  ------ | | --------
	seed = random.randint(100000,999999)
	print seed
	pn = perlin.SimplexNoise(seed)	#seed ##42046
	for x in range(0,xlen):
		map.append ([])
		for y in range (0,ylen):
			xos = x-xcen
			yos = y-ycen
			Amp = 100+pn.noise2((x*.9)/100,(y*.9)/100)*155
			noise = (pn.noise2(x*Xsca/100,y*Ysca/100)*Amp+pn.noise2(x*Xsca/10,y*Ysca/10)*Amp/2+pn.noise2(x*Xsca,y*Ysca)*Amp/4)
			continent = (-(float(xos)**2)/(xlen+cw)+cw-(float(yos)**2)/(ylen+cl)+cl)*2
			map[x].append(Tile(x,y,continent+noise))
			#map[x].append(Tile(x,y,-10))
	return map
def world_gen_shitfuck():
	global xlen
	global ylen
	global size
	global map_size

	size = 5
	xlen = 640
	ylen = 480
	xcen = xlen/2
	ycen = ylen/2
	map_size = [xlen,ylen]
	map = []
	Xsca = 1
	Ysca = 1
	Ampsca =  random.uniform(-.1,.1)*10
	#Ampsca =  0
	seed = random.randint(10000,100000)
	#seed = 0
	#print seed
	#print Ampsca
	pn = perlin.SimplexNoise(seed)	#seed ##42046
	for x in range(0,xlen):
		map.append ([])
		for y in range (0,ylen):
			Amp = pn.noise2((x*Ampsca)/100,(y*Ampsca)/100)*255
			map[x].append(Tile(x,y,(pn.noise2(x*Xsca/100,y*Ysca/100)*Amp+pn.noise2(x*Xsca/10,y*Ysca/10)*Amp/2+pn.noise2(x*Xsca,y*Ysca)*Amp/4)))
	
	return map